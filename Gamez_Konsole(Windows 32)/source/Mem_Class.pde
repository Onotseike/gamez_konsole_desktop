
import apwidgets.*;




public class Mem //extends PWidgetContainer
{

  int w, h, moves, counter, holder, holder_r, holder_c;
  IntList numlist, exposed, img_b, img_c ;
  PImage[] bcks = new PImage[5];
  PImage[] card = new PImage[2];
  PImage ova;
  //PImage card_back1, card_back2, Mem_Back;


  //constructor
  Mem(int W, int H) 
  {

    //card_back1 = loadImage("card_back1.jpg");
    //card_back2 = loadImage("card_back2.jpg");
    //Mem_Back = loadImage("bckgrnd.jpg");
    for (int i = 0 ; i < bcks.length; i++)
    {
      bcks[i] = loadImage("bck"+str(i+2)+".png");
    }
    for (int i = 0; i <card.length; i++)
    {
      card[i] = loadImage("card_back"+str(i+1)+".jpg");
    }
    ova = loadImage("mem.png");
    w = W;
    h = H;
    moves = 0;

    numlist = new IntList();
    exposed = new IntList();
    img_b = new IntList();
    img_c = new IntList();
    counter = 0;
    holder = 0;
    holder_r = 0;
    holder_c = 0;

    for (int i= 0; i < 8 ; i++)
    {
      numlist.append(i);
      println(numlist);
    }
    for ( int I = 7 ; I>=0 ; I--)
    {
      numlist.append(I);
      println(numlist);
    }

    for (int i = 0; i < 16; i++)
    {
      exposed.append(8);
    }

    numlist.shuffle();
    println(numlist);
    for (int i = 0 ; i <5; i++)
    {
      img_b.append(i);
    }
    img_b.shuffle();
    for (int i = 0 ; i < 2 ; i++)
    {
      img_c.append(i);
    }
    img_c.shuffle();
  }
  // display function

  void display()
  {

    image(bcks[img_b.get(0)], 0, 0, displayWidth, displayHeight);

    for (int r = 0; r < 4 ; r++)
    {
      for (int c = 0 ; c < 4  ; c ++) //&& for (int i = 0; i < 16; i ++)
      {
        pushStyle();
        textAlign(CENTER);
        textSize(50);
        fill(0);
        text( str(numlist.get((4*r)+c)), 60+(displayWidth/4)*r, 20 + displayHeight/4 + (displayHeight/6)*c ); 
        popStyle();


        pushStyle();
        noFill();
        stroke(255);
        rect ((displayWidth/4 )* r, displayHeight/6 + (displayHeight/6)*c, displayWidth/4, displayHeight/6);
        image(card[img_c.get(0)], (displayWidth/4 )* r, displayHeight/6 + (displayHeight/6)*c, displayWidth/4, displayHeight/6); 
        popStyle();
      }
    }

    pushStyle();
    fill(255);
    rect(displayWidth/20, displayHeight/30, displayWidth/3, displayHeight/20);
    popStyle();
    pushStyle();
    textSize(20);
    fill(0);
    text("MOVES: ", 5+ displayWidth/20, 20+displayHeight/30); 
    popStyle();

//    rect(displayWidth/3, 6*(displayHeight/7), displayWidth/3, displayHeight/10);
//    pushStyle();
//    fill(0);
//    textSize(20);
//    text("RESTART", 5 + displayWidth/3, 20+ 6*(displayHeight/7), displayWidth/3, displayHeight/10);
//    popStyle();
  }

  void Restart()
  {
    display();
  }



  void click()
  {
    /*if (mousePressed == true)
     { 
     float x = 0;
     x = constrain(x,displayWidth/3, 2* (displayWidth/3));
     float y = 0; 
     y = constrain(y, 6*(displayHeight/7), displayHeight/10);
     if ( mouseX == x && mouseY == y)
     {
     Restart();
     }
     
     }*/
    for ( int i = 0 ; i < 4 ; i++)
    {
      if (mouseX > 0 && mouseX < displayWidth/4)
      {
        if (mouseY > ( displayHeight/6 + (displayHeight/6)*i)  && mouseY < (displayHeight/6 + (displayHeight/6)*(1+i)))
        { 
          if (mousePressed == true)
          {
            if (counter == 0)
            {

              holder = numlist.get(i);
              holder_c = i;
              holder_r = 0;
              println("HR" + holder_r);
              println("H  " + holder);
              counter = 1;
              println("Counter" + counter);
              exposed.set(i, numlist.get(i));
              println("exposed" + exposed);


              pushStyle();
              textAlign(CENTER);
              textSize(50);
              fill(200, 133, 100);
              text( str(numlist.get(i)), 60+(displayWidth/4)*0, 20 + displayHeight/4 + (displayHeight/6)*i ); 
              popStyle();
            }
            else if (counter == 1)
            {
              moves = moves + 1;
              rect(displayWidth/20, displayHeight/30, displayWidth/3, displayHeight/20);
              pushStyle();
              fill(0);
              textSize(20);
              text("MOVES =  " + str(moves), 5+ displayWidth/20, 20+displayHeight/30);
              popStyle();
              //rect(displayWidth/20, displayHeight/30, displayWidth/3, displayHeight/30);
              counter = 0;
              println("Counter"+counter);
              exposed.set(i, numlist.get(i));
              println("exposed" + exposed);

              if (numlist.get(i) == holder)
              {
                pushStyle();
                textAlign(CENTER);
                textSize(50);
                fill(200, 133, 100);
                text( str(numlist.get(i)), 60+(displayWidth/4)*0, 20 + displayHeight/4 + (displayHeight/6)*i ); 
                popStyle();
              }
              else
              {
                pushStyle();
                noFill();
                stroke(255);
                rect ((displayWidth/4 )* 0, displayHeight/6 + (displayHeight/6)*i, displayWidth/4, displayHeight/6);
                image(card[img_c.get(0)], (displayWidth/4 )* 0, displayHeight/6 + (displayHeight/6)*i, displayWidth/4, displayHeight/6);
                popStyle();
                pushStyle();
                noFill();
                stroke(255);
                rect ((displayWidth/4 )* holder_r, displayHeight/6 + (displayHeight/6)*holder_c, displayWidth/4, displayHeight/6);
                image(card[img_c.get(0)], (displayWidth/4 )* holder_r, displayHeight/6 + (displayHeight/6)*holder_c, displayWidth/4, displayHeight/6);
                popStyle();
              }
            }

            //background(0);
          }
        }
      }
      else if (mouseX > displayWidth/4 && mouseX < (displayWidth/2))
      {
        if (mouseY > ( displayHeight/6 + (displayHeight/6)*i)  && mouseY < (displayHeight/6 + (displayHeight/6)*(1+i)))
        {
          int col = 1;
          if (mousePressed == true)
          {
            if (counter == 0)
            {

              holder = numlist.get((4*col)+i);
              holder_c = i;
              holder_r = 1;
              println("HR " + holder_r);
              println("H  " + holder);
              counter = 1;
              println("Counter" + counter);
              exposed.set((4*col)+i, numlist.get((4*col)+i));
              println("exposed" + exposed);


              pushStyle();
              textAlign(CENTER);
              textSize(50);
              fill(200, 133, 100);
              text( str(numlist.get((4*col)+i)), 60+(displayWidth/4)*col, 20 + displayHeight/4 + (displayHeight/6)*i ); 
              popStyle();
            }
            else if (counter == 1)
            {
              moves = moves + 1;
              rect(displayWidth/20, displayHeight/30, displayWidth/3, displayHeight/20);
              pushStyle();
              fill(0);
              textSize(20);
              text("MOVES =  " + str(moves), 5+ displayWidth/20, 20+displayHeight/30);
              popStyle();
              counter = 0;
              println("Counter"+counter);
              exposed.set((4*col)+i, numlist.get((4*col)+i));
              println("exposed" + exposed);

              if (numlist.get((4*col)+i) == holder)
              {
                pushStyle();
                textAlign(CENTER);
                textSize(50);
                fill(200, 133, 100);
                text( str(numlist.get((4*col)+i)), 60+(displayWidth/4)*col, 20 + displayHeight/4 + (displayHeight/6)*i ); 
                popStyle();
              }
              else
              {
                pushStyle();
                noFill();
                stroke(255);
                rect ((displayWidth/4 )* 1, displayHeight/6 + (displayHeight/6)*i, displayWidth/4, displayHeight/6);
                image(card[img_c.get(0)], (displayWidth/4 )* 1, displayHeight/6 + (displayHeight/6)*i, displayWidth/4, displayHeight/6);
                popStyle();
                pushStyle();
                noFill();
                stroke(255);
                rect ((displayWidth/4 )* holder_r, displayHeight/6 + (displayHeight/6)*holder_c, displayWidth/4, displayHeight/6);
                image(card[img_c.get(0)], (displayWidth/4 )* holder_r, displayHeight/6 + (displayHeight/6)*holder_c, displayWidth/4, displayHeight/6);
                popStyle();
              }
            }
          }
        }
      }
      else if (mouseX > displayWidth/2 && mouseX < 3*(displayWidth/4))
      {
        if (mouseY > ( displayHeight/6 + (displayHeight/6)*i)  && mouseY < (displayHeight/6 + (displayHeight/6)*(1+i)))
        { 
          int col = 2;
          if (mousePressed == true)
          {
            if (counter == 0)
            {

              holder = numlist.get((4*col)+i);
              holder_c = i;
              holder_r = 2;
              println("HR " + holder_r);
              println("H  " + holder);
              counter = 1;
              println("Counter" + counter);
              exposed.set((4*col)+i, numlist.get((4*col)+i));
              println("exposed" + exposed);

              pushStyle();
              textAlign(CENTER);
              textSize(50);
              fill(200, 133, 100);
              text( str(numlist.get((4*col)+i)), 60+(displayWidth/4)*col, 20 + displayHeight/4 + (displayHeight/6)*i ); 
              popStyle();
            }

            //background(0);
            else if (counter == 1)
            {
              moves = moves + 1;
              rect(displayWidth/20, displayHeight/30, displayWidth/3, displayHeight/20);
              pushStyle();
              fill(0);
              textSize(20);
              text("MOVES =  " + str(moves), 5+ displayWidth/20, 20+displayHeight/30);
              popStyle();
              counter = 0;
              println("Counter"+counter);
              exposed.set((4*col)+i, numlist.get((4*col)+i));
              println("exposed" + exposed);

              if (numlist.get((4*col)+i) == holder)
              {
                pushStyle();
                textAlign(CENTER);
                textSize(50);
                fill(200, 133, 100);
                text( str(numlist.get((4*col)+i)), 60+(displayWidth/4)*col, 20 + displayHeight/4 + (displayHeight/6)*i ); 
                popStyle();
              }
              else
              {
                pushStyle();
                noFill();
                stroke(255);
                rect ((displayWidth/4 )* 1, displayHeight/6 + (displayHeight/6)*i, displayWidth/4, displayHeight/6);
                image(card[img_c.get(0)], (displayWidth/4 )* 1, displayHeight/6 + (displayHeight/6)*i, displayWidth/4, displayHeight/6);
                popStyle();
                pushStyle();
                noFill();
                stroke(255);
                rect ((displayWidth/4 )* holder_r, displayHeight/6 + (displayHeight/6)*holder_c, displayWidth/4, displayHeight/6);
                image(card[img_c.get(0)], (displayWidth/4 )* holder_r, displayHeight/6 + (displayHeight/6)*holder_c, displayWidth/4, displayHeight/6);
                popStyle();
              }
            }
          }
        }
      }

      else if (mouseX > 3*(displayWidth/4) && mouseX < displayWidth)
      {
        if (mouseY > ( displayHeight/6 + (displayHeight/6)*i)  && mouseY < (displayHeight/6 + (displayHeight/6)*(1+i)))
        { 
          int col = 3;
          if (mousePressed == true)
          {
            if (counter == 0)
            {

              holder = numlist.get((4*col)+i);
              holder_c = i;
              holder_r = 3;
              println("HR " + holder_r);
              println("H  " + holder);
              counter = 1;
              println("Counter" + counter);
              exposed.set((4*col)+i, numlist.get((4*col)+i));
              println("exposed" + exposed);

              pushStyle();
              textAlign(CENTER);
              textSize(50);
              fill(200, 133, 100);
              text( str(numlist.get((4*col)+i)), 60+(displayWidth/4)*col, 20 + displayHeight/4 + (displayHeight/6)*i ); 
              popStyle();
            }

            else if (counter == 1)
            {
              moves = moves + 1;
              rect(displayWidth/20, displayHeight/30, displayWidth/3, displayHeight/20);
              pushStyle();
              fill(0);
              textSize(20);
              text("MOVES =  " + str(moves), 5+ displayWidth/20, 20+displayHeight/30);
              popStyle();
              counter = 0;
              println("Counter"+counter);
              exposed.set((4*col)+i, numlist.get((4*col)+i));
              println("exposed" + exposed);

              if (numlist.get((4*col)+i) == holder)
              {
                pushStyle();
                textAlign(CENTER);
                textSize(50);
                fill(200, 133, 100);
                text( str(numlist.get((4*col)+i)), 60+(displayWidth/4)*col, 20 + displayHeight/4 + (displayHeight/6)*i ); 
                popStyle();
              }
              else
              {
                pushStyle();
                noFill();
                stroke(255);
                rect ((displayWidth/4 )* 1, displayHeight/6 + (displayHeight/6)*i, displayWidth/4, displayHeight/6);
                image(card[img_c.get(0)], (displayWidth/4 )* 1, displayHeight/6 + (displayHeight/6)*i, displayWidth/4, displayHeight/6);
                popStyle();
                pushStyle();
                noFill();
                stroke(255);
                rect ((displayWidth/4 )* holder_r, displayHeight/6 + (displayHeight/6)*holder_c, displayWidth/4, displayHeight/6);
                image(card[img_c.get(0)], (displayWidth/4 )* holder_r, displayHeight/6 + (displayHeight/6)*holder_c, displayWidth/4, displayHeight/6);
                popStyle();
              }
            }
          }
        }
      }
    }


    if (exposed.hasValue(8) == false)
    {
      println(" Game Over");
      //image(bck4,0,0,displayWidth,displayHeight);
      image(ova,0,displayHeight/4,displayWidth,displayHeight/4);
    }
  }
}

