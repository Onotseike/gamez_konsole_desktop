void reset() {
  nave = new Nave(200, width/2);
  misiles = new ArrayList<Misil>();
  velocidad = 1;
  vidas = 3;
  puntos = 0;
  contBonus = 0;
  atractor = 0;
  tokenVida = false;
  tokenAtractor = false;
}


void tablero() {
  fill(255);
  textSize(30);
  text(puntos, 10, 40);

  fill(255, 0, 0);
  text(contBonus, 10, height-10);

  for (int i=0; i<vidas; i++) {
    fill(255, 0, 0);
    ellipse(20+i*20, 60, 10, 10);
  }
}

void sp_draw()
{
  if (frameCount % 5 == 0) {
    misiles.add(new Misil(width+10, random(height), 0));
  }
  if (frameCount % 60 == 0) {
    misiles.add(new Misil(width+10, random(height), 1)); //bonus
  }
  if (tokenVida) {
    misiles.add(new Misil(width+10, random(height), 2)); //vida
    tokenVida = false;
  }
  if (tokenAtractor) {
    misiles.add(new Misil(width+10, random(height), 3)); //atractor
    tokenAtractor = false;
  }

  if (vidas > 0) {
    nave.draw();

    for (int i=0; i < misiles.size(); i++) {
      Misil tal = misiles.get(i);
      tal.ataque();
      tal.draw();

      int t = nave.verChoque(tal);
      switch(t) {
      case 0 : //misil
        if (atractor == 0) {
          vidas--;
          contBonus = 0;
          nave.reset();
        }
        misiles.remove(i);
        break;
      case 1 : //bonus
        puntos = puntos + bonus;
        contBonus++; 
        if (contBonus % 2 == 0) tokenVida = true;
        if (contBonus % 5 == 0) tokenAtractor = true;
        misiles.remove(i);
        break;
      case 2 : //vida
        vidas++;
        misiles.remove(i);
        break;
      case 3 : //atractor
        atractor += 1000;
        misiles.remove(i);
        break;
      }

      if (atractor>0) {
        if (tal.getTipo()==1) {
          tal.atractor(nave, 1); //entre nave y bonuses
          atractor--;
        }
      }

      if (tal.paso()) {
        misiles.remove(i);
      }
    }
  }
  else reset();

  tablero();

  if (second() % 60 < 5)
    velocidad += 0.005;

  println(velocidad);
}
