public class Ayo
{
  //Fields
  PImage[] ayo_img = new PImage[4];
  PFont font;
  int P1, P2, c1, c2, click1, click2, click3, click4, click5, click6, click7, click8, click9, click10, click11, click12;
  IntList hole;
  boolean started = true;
  boolean inplay = false;
  float player = random(0, 1);
  String turn1 = "PLAYER 1";
  String turn2 = "PLAYER 2";
  String turn = "";


  // Methods

  // constructor
  Ayo()
  {
    font = loadFont("Bauhaus93-48.vlw");
    ayo_img[0] = loadImage("Ayoo.jpg");
    ayo_img[1]=loadImage("Ayo_Board.png");
    ayo_img[2] = loadImage("Ayo_seed.png");
    ayo_img[3] = loadImage("plain_board.png");

    P1 = 0; 
    P2= 0; 
    c1 =0; 
    c2 = 0;
    click1 = 0;
    click2 = 0;
    click3 = 0;
    click4 = 0;
    click5 = 0;
    click6 = 0;
    click7 = 0;
    click8 = 0;
    click9 = 0;
    click10 = 0;
    click11 = 0;
    click12 = 0;
    hole = new IntList();

    for (int i = 0 ; i < 12 ; i++)
    {
      hole.append(1);
    }
    println(hole);
  }

  // Properties function
  void display()
  {

    image(ayo_img[0], 0, 0, displayWidth, displayHeight);
    image(ayo_img[1], displayWidth/7, displayHeight/4, 5*(displayWidth/7), 2*(displayHeight/4));
    image(ayo_img[3], 0, 3* (displayHeight/8), displayWidth/7, displayHeight/4);
    image(ayo_img[3], 6*(displayWidth/7), 3*(displayHeight/8), displayWidth/7, displayHeight/4);

    pushStyle();
    noStroke();
    fill(205, 133, 63);    
    //hints
    rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
    rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
    // Players
    rect(0, 0, displayWidth/5, displayHeight/10);
    rect(4*(displayWidth/5), 9*(displayHeight/10), displayWidth/5, displayHeight/10);
    // Score
    rect(displayWidth/28, displayHeight/4, displayWidth/14, displayHeight/8);
    rect(25.5*(displayWidth/28), displayHeight/4, (displayWidth/14), displayHeight/8);

    for ( int r =0; r < 6; r++)
    {
      rect(displayWidth/5.5+ (r* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
      rect(displayWidth/5.5+ (r* displayWidth/8.6), 3*(displayHeight/4), displayWidth/18, displayHeight/8);
    }

    popStyle();

    pushStyle();
    textFont(font);
    textSize(40);
    fill(0);
    text("PLAYER 1", 4, 30);
    text("PLAYER 2", 4+ 4*(displayWidth/5), 30+ 9*(displayHeight/10));
    pushStyle();

    if ( player >= 0 && player < 0.5)
    {
      turn = turn1;
      pushStyle();
      fill(0);
      textFont(font);
      textSize(20);
      text( turn + " Start", 2*displayWidth/3, 30);
      popStyle();
    }
    else if ( player > 0.5 && player <= 1)
    {
      turn = turn2;
      pushStyle();
      fill(0);
      textSize(20);
      text(turn + "  Start", displayWidth/6, 30+9*displayHeight/10);
      popStyle();
    }


    if (started)
    {

      // for hole a  
      image(ayo_img[2], 5.0*displayWidth/28, 2.6*displayHeight/8, 20, 20);
      image(ayo_img[2], 5.0* displayWidth/28, 3.3*displayHeight/8, 20, 20);
      image(ayo_img[2], 6.3*displayWidth/28, 2.6*displayHeight/8, 20, 20);
      image(ayo_img[2], 6.3*displayWidth/28, 3.3*displayHeight/8, 20, 20);
      hole.set(0, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(0), 10+ displayWidth/5.5 + 0*displayWidth/8.6, 40 + displayHeight/8);
      popStyle();


      // for hole b
      image(ayo_img[2], 5.0*displayWidth/28, 4.5*displayHeight/8, 20, 20);
      image(ayo_img[2], 5.0*displayWidth/28, 5.2*displayHeight/8, 20, 20);
      image(ayo_img[2], 6.3*displayWidth/28, 4.5*displayHeight/8, 20, 20);
      image(ayo_img[2], 6.3*displayWidth/28, 5.2*displayHeight/8, 20, 20);
      hole.set(1, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(1), 10 + displayWidth/5.5 + 0*displayWidth/8.6, 40 + 3*displayHeight/4);
      popStyle();

      // for hole c
      image(ayo_img[2], 8.1*displayWidth/28, 2.6*displayHeight/8, 20, 20);
      image(ayo_img[2], 8.1*displayWidth/28, 3.3*displayHeight/8, 20, 20);
      image(ayo_img[2], 9.3*displayWidth/28, 2.6*displayHeight/8, 20, 20);
      image(ayo_img[2], 9.3*displayWidth/28, 3.3*displayHeight/8, 20, 20);
      hole.set(2, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(2), 10 + displayWidth/5.5 + 1*displayWidth/8.6, 40 + displayHeight/8);
      popStyle();

      // for hole d
      image(ayo_img[2], 8.1*displayWidth/28, 4.5*displayHeight/8, 20, 20);
      image(ayo_img[2], 8.1*displayWidth/28, 5.2*displayHeight/8, 20, 20);
      image(ayo_img[2], 9.3*displayWidth/28, 4.5*displayHeight/8, 20, 20);
      image(ayo_img[2], 9.3*displayWidth/28, 5.2*displayHeight/8, 20, 20);
      hole.set(3, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(3), 10 + displayWidth/5.5 + 1*displayWidth/8.6, 40 + 3*displayHeight/4);
      popStyle();

      //for hole e
      image(ayo_img[2], 11.4*displayWidth/28, 2.6*displayHeight/8, 20, 20);
      image(ayo_img[2], 11.4*displayWidth/28, 3.3*displayHeight/8, 20, 20);
      image(ayo_img[2], 12.6*displayWidth/28, 2.6*displayHeight/8, 20, 20);
      image(ayo_img[2], 12.6*displayWidth/28, 3.3*displayHeight/8, 20, 20);
      hole.set(4, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(4), 10 + displayWidth/5.5 + 2*displayWidth/8.6, 40 + displayHeight/8);
      popStyle();

      // for hole f
      image(ayo_img[2], 11.4*displayWidth/28, 4.5*displayHeight/8, 20, 20);
      image(ayo_img[2], 11.4*displayWidth/28, 5.2*displayHeight/8, 20, 20);
      image(ayo_img[2], 12.6*displayWidth/28, 4.5*displayHeight/8, 20, 20);
      image(ayo_img[2], 12.6*displayWidth/28, 5.2*displayHeight/8, 20, 20);
      hole.set(5, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(5), 10 + displayWidth/5.5 + 2*displayWidth/8.6, 40 + 3*displayHeight/4);
      popStyle();

      // for hole g
      image(ayo_img[2], 14.8*displayWidth/28, 2.6*displayHeight/8, 20, 20);
      image(ayo_img[2], 14.8*displayWidth/28, 3.3*displayHeight/8, 20, 20);
      image(ayo_img[2], 16*displayWidth/28, 2.6*displayHeight/8, 20, 20);
      image(ayo_img[2], 16*displayWidth/28, 3.3*displayHeight/8, 20, 20);
      hole.set(6, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(6), 10 + displayWidth/5.5 + 3*displayWidth/8.6, 40 + displayHeight/8);
      popStyle();

      // for hole h
      image(ayo_img[2], 14.8*displayWidth/28, 4.5*displayHeight/8, 20, 20);
      image(ayo_img[2], 14.8*displayWidth/28, 5.2*displayHeight/8, 20, 20);
      image(ayo_img[2], 16*displayWidth/28, 4.5*displayHeight/8, 20, 20);
      image(ayo_img[2], 16*displayWidth/28, 5.2*displayHeight/8, 20, 20);
      hole.set(7, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(7), 10 + displayWidth/5.5 + 3*displayWidth/8.6, 40 + 3*displayHeight/4);
      popStyle();

      // for hole i
      image(ayo_img[2], 18*displayWidth/28, 2.6*displayHeight/8, 20, 20);
      image(ayo_img[2], 18*displayWidth/28, 3.3*displayHeight/8, 20, 20);
      image(ayo_img[2], 19.2*displayWidth/28, 2.6*displayHeight/8, 20, 20);
      image(ayo_img[2], 19.2*displayWidth/28, 3.3*displayHeight/8, 20, 20);
      hole.set(8, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(8), 10 + displayWidth/5.5 + 4*displayWidth/8.6, 40 + displayHeight/8);
      popStyle();

      // for hole j
      image(ayo_img[2], 18*displayWidth/28, 4.5*displayHeight/8, 20, 20);
      image(ayo_img[2], 18*displayWidth/28, 5.2*displayHeight/8, 20, 20);
      image(ayo_img[2], 19.2*displayWidth/28, 4.5*displayHeight/8, 20, 20);
      image(ayo_img[2], 19.2*displayWidth/28, 5.2*displayHeight/8, 20, 20);
      hole.set(9, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(9), 10 + displayWidth/5.5 + 4*displayWidth/8.6, 40 + 3*displayHeight/4);
      popStyle();

      // for hole k
      image(ayo_img[2], 21*displayWidth/28, 2.6*displayHeight/8, 20, 20);
      image(ayo_img[2], 21*displayWidth/28, 3.3*displayHeight/8, 20, 20);
      image(ayo_img[2], 22.2*displayWidth/28, 2.6*displayHeight/8, 20, 20);
      image(ayo_img[2], 22.2*displayWidth/28, 3.3*displayHeight/8, 20, 20);
      hole.set(10, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(10), 10 + displayWidth/5.5 + 5*displayWidth/8.6, 40 + displayHeight/8);
      popStyle();

      // for hole l
      image(ayo_img[2], 21*displayWidth/28, 4.5*displayHeight/8, 20, 20);
      image(ayo_img[2], 21*displayWidth/28, 5.2*displayHeight/8, 20, 20);
      image(ayo_img[2], 22.2*displayWidth/28, 4.5*displayHeight/8, 20, 20);
      image(ayo_img[2], 22.2*displayWidth/28, 5.2*displayHeight/8, 20, 20);
      hole.set(11, 4);
      pushStyle();
      textSize(30);
      fill(0);
      text(hole.get(11), 10 + displayWidth/5.5 + 5*displayWidth/8.6, 40 + 3*displayHeight/4);
      popStyle();

      started = false;
    }
  }
  void clicks()
  {
    if (mousePressed == true)
    {
      if ( mouseX > 95.9* displayWidth/588 && mouseX < 151.9* displayWidth/588 && mouseY > 5.2* displayHeight/16 && mouseY < 7.2* displayHeight/16)
      {
        float ax = constrain(mouseX, 95.9* displayWidth/588, 151.9* displayWidth/588);
        float ay = constrain(mouseY, 5.2* displayHeight/16, 7.2* displayHeight/16);
        image(ayo_img[2], ax, ay, 20, 20);
      }

      if ( mouseX > 95.9* displayWidth/588 && mouseX < 151.9* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
      {
        float bx = constrain(mouseX, 95.9* displayWidth/588, 151.9* displayWidth/588);
        float by = constrain(mouseY, 9* displayHeight/16, 11* displayHeight/16);
        image(ayo_img[2], bx, by, 20, 20);
      }
      if ( mouseX > 161* displayWidth/588 && mouseX < 217* displayWidth/588 && mouseY > 5.2* displayHeight/16 && mouseY < 7.2* displayHeight/16)
      {
        float cx = constrain(mouseX, 161* displayWidth/588, 217* displayWidth/588);
        float cy = constrain(mouseY, 5.2* displayHeight/16, 7.2* displayHeight/16);
        image(ayo_img[2], cx, cy, 20, 20);
      }
      if ( mouseX > 161* displayWidth/588 && mouseX < 217* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
      {
        float dx = constrain(mouseX, 161* displayWidth/588, 217* displayWidth/588);
        float dy = constrain(mouseY, 9* displayHeight/16, 11* displayHeight/16);
        image(ayo_img[2], dx, dy, 20, 20);
      }
      if ( mouseX > 230.3* displayWidth/588 && mouseX < 286.3* displayWidth/588 && mouseY > 5.2* displayHeight/16 && mouseY < 7.2* displayHeight/16)
      {
        float ex = constrain(mouseX, 230.3* displayWidth/588, 286.3* displayWidth/588);
        float ey = constrain(mouseY, 5.2* displayHeight/16, 7.2* displayHeight/16);
        image(ayo_img[2], ex, ey, 20, 20);
      }
      if ( mouseX > 230.3* displayWidth/588 && mouseX < 286.3* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
      {
        float fx = constrain(mouseX, 230.3* displayWidth/588, 286.3* displayWidth/588);
        float fy = constrain(mouseY, 9* displayHeight/16, 11* displayHeight/16);
        image(ayo_img[2], fx, fy, 20, 20);
      }
      if ( mouseX > 305.9* displayWidth/588 && mouseX < 361.9* displayWidth/588 && mouseY > 5.2* displayHeight/16 && mouseY < 7.2* displayHeight/16)
      {
        float gx = constrain(mouseX, 305.9* displayWidth/588, 361.9* displayWidth/588);
        float gy = constrain(mouseY, 5.2* displayHeight/16, 7.2* displayHeight/16);
        image(ayo_img[2], gx, gy, 20, 20);
      }
      if ( mouseX > 305.9* displayWidth/588 && mouseX < 361.9* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
      {
        float hx = constrain(mouseX, 305.9* displayWidth/588, 361.9* displayWidth/588);
        float hy = constrain(mouseY, 9* displayHeight/16, 11* displayHeight/16);
        image(ayo_img[2], hx, hy, 20, 20);
      }
      if ( mouseX > 368.9* displayWidth/588 && mouseX < 424.9* displayWidth/588 && mouseY > 5.2* displayHeight/16 && mouseY < 7.2* displayHeight/16)
      {
        float ix = constrain(mouseX, 368.9* displayWidth/588, 424.9* displayWidth/588);
        float iy = constrain(mouseY, 5.2* displayHeight/16, 7.2* displayHeight/16);
        image(ayo_img[2], ix, iy, 20, 20);
      }
      if ( mouseX > 368.9* displayWidth/588 && mouseX < 424.9* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
      {
        float jx = constrain(mouseX, 368.9* displayWidth/588, 424.9* displayWidth/588);
        float jy = constrain(mouseY, 9* displayHeight/16, 11* displayHeight/16);
        image(ayo_img[2], jx, jy, 20, 20);
      }
      if ( mouseX > 31* displayWidth/42 && mouseX < 5* displayWidth/6 && mouseY > 5.2* displayHeight/16 && mouseY < 7.2* displayHeight/16)
      {
        float kx = constrain(mouseX, 31* displayWidth/42, 5* displayWidth/6);
        float ky = constrain(mouseY, 5.2* displayHeight/16, 7.2* displayHeight/16);
        image(ayo_img[2], kx, ky, 20, 20);
      }
      if ( mouseX > 31* displayWidth/42 && mouseX < 5* displayWidth/6  && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
      {
        float lx = constrain(mouseX, 31* displayWidth/42, 5* displayWidth/6);
        float ly = constrain(mouseY, 9* displayHeight/16, 11* displayHeight/16);
        image(ayo_img[2], lx, ly, 20, 20);
      }
    }

    if ( mouseX > 95.9* displayWidth/588 && mouseX < 151.9* displayWidth/588 && mouseY > 5.2* displayHeight/16 && mouseY < 7.2* displayHeight/16)
    {
      if (click1 ==1)
      {
        fill(205, 133, 63);
        rect(displayWidth/5.5+ (0* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(0, hole.get(0)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(0) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(0), 10+ displayWidth/5.5 + 0*displayWidth/8.6, 40 + displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(0) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }

          // rect(displayWidth/5.5+ (r* displayWidth/8.6), 3*(displayHeight/4), displayWidth/18, displayHeight/8);
        }
        click1 -= 1;
      }
      else if (click1 == 0)
      {
        controla();
        click1 +=1;
        //click1 = 1;
        click2 = 1;
        click3 = 1;
        click4 = 1;
        click5 = 1;
        click6 = 1;
        click7 = 1;
        click8 = 1;
        click9 = 1;
        click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }


    if ( mouseX > 95.9* displayWidth/588 && mouseX < 151.9* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
    {
      if ( click2 == 1)
      {
        fill(205, 133, 63);
        rect(displayWidth/5.5+ (0* displayWidth/8.6), 3*(displayHeight/4), displayWidth/18, displayHeight/8);
        //rect(displayWidth/5.5+ (0* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(1, hole.get(1)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(1) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(1), 10+ displayWidth/5.5 + 0*displayWidth/8.6, 40 + 6*displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(1) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click2 -= 1;
      }
      else if (click2 == 0) 
      {
        controlb();
        click2 += 1;
        click1 = 1;
        //click2 = 1;
        click3 = 1;
        click4 = 1;
        click5 = 1;
        click6 = 1;
        click7 = 1;
        click8 = 1;
        click9 = 1;
        click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }


    if ( mouseX > 161* displayWidth/588 && mouseX < 217* displayWidth/588 && mouseY > 5.2* displayHeight/16 && mouseY < 7.2* displayHeight/16)
    {

      if (click3 == 1)
      {

        fill(205, 133, 63);
        rect(displayWidth/5.5+ (1* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(2, hole.get(2)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(2)); 

        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(2), 10+ displayWidth/5.5 + 1*displayWidth/8.6, 40 + displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(2) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click3 -= 1;
      }
      else if (click3 == 0)
      {
        controlc();
        click3 += 1;
        click1 = 1;
        click2 = 1;
        //click3 = 1;
        click4 = 1;
        click5 = 1;
        click6 = 1;
        click7 = 1;
        click8 = 1;
        click9 = 1;
        click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }


    if ( mouseX > 161* displayWidth/588 && mouseX < 217* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
    {
      if (click4 == 1)
      {


        fill(205, 133, 63);
        rect(displayWidth/5.5+ (1* displayWidth/8.6), 3*(displayHeight/4), displayWidth/18, displayHeight/8);
        //rect(displayWidth/5.5+ (0* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(3, hole.get(3)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(3) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(3), 10+ displayWidth/5.5 + 1*displayWidth/8.6, 40 + 6*displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(3) == 1 && c1 == c2)
        {

          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click4 -= 1;
      }
      else if (click4 == 0)
      {
        controld();
        click4 += 1;
        click1 = 1;
        click2 = 1;
        click3 = 1;
        //click4 = 1;
        click5 = 1;
        click6 = 1;
        click7 = 1;
        click8 = 1;
        click9 = 1;
        click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }


    if ( mouseX > 230.3* displayWidth/588 && mouseX < 286.3* displayWidth/588 && mouseY > 5.2* displayHeight/16 && mouseY < 7.2* displayHeight/16)
    {
      if (click5 == 1)
      {

        fill(205, 133, 63);
        rect(displayWidth/5.5+ (2* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(4, hole.get(4)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(4));
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(4), 10+ displayWidth/5.5 + 2*displayWidth/8.6, 40 + displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(4) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click5 -= 1;
      }
      else if (click5 == 0)
      { 
        controle();
        click5 += 1;
        click1 = 1;
        click2 = 1;
        click3 = 1;
        click4 = 1;
        // click5 = 1;
        click6 = 1;
        click7 = 1;
        click8 = 1;
        click9 = 1;
        click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }


    if ( mouseX > 230.3* displayWidth/588 && mouseX < 286.3* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
    {
      if (click6 == 1)
      {

        fill(205, 133, 63);
        rect(displayWidth/5.5+ (2* displayWidth/8.6), 3*(displayHeight/4), displayWidth/18, displayHeight/8);
        //rect(displayWidth/5.5+ (0* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(5, hole.get(5)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(5) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(5), 10+ displayWidth/5.5 + 2*displayWidth/8.6, 40 + 6*displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(5) == 1 && c1 == c2)
        {

          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click6 -= 1;
      }
      else if (click6 == 0)
      {
        controlf();
        click6 += 1;
        click1 = 1;
        click2 = 1;
        click3 = 1;
        click4 = 1;
        click5 = 1;
        //click6 = 1;
        click7 = 1;
        click8 = 1;
        click9 = 1;
        click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }


    if ( mouseX > 305.9* displayWidth/588 && mouseX < 361.9* displayWidth/588 && mouseY > 5.2* displayHeight/16 && mouseY < 7.2* displayHeight/16)
    {
      if (click7 == 1)
      {
        fill(205, 133, 63);
        rect(displayWidth/5.5+ (3* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(6, hole.get(6)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(6) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(6), 10+ displayWidth/5.5 + 3*displayWidth/8.6, 40 + displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(6) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click7 -=1;
      }
      else if (click7 == 0)
      {
        controlg();
        click7 += 1;
        click1 = 1;
        click2 = 1;
        click3 = 1;
        click4 = 1;
        click5 = 1;
        click6 = 1;
        //click7 = 1;
        click8 = 1;
        click9 = 1;
        click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }


    if ( mouseX > 305.9* displayWidth/588 && mouseX < 361.9* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
    {
      if (click8 == 1)
      {

        fill(205, 133, 63);
        rect(displayWidth/5.5+ (3* displayWidth/8.6), 3*(displayHeight/4), displayWidth/18, displayHeight/8);
        //rect(displayWidth/5.5+ (0* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(7, hole.get(7)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(7) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(7), 10+ displayWidth/5.5 + 3*displayWidth/8.6, 40 + 6*displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(7) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click8 -=1;
      }
      else if (click8 == 0)
      {
        controlh();
        click8 += 1;
        click1 = 1;
        click2 = 1;
        click3 = 1;
        click4 = 1;
        click5 = 1;
        click6 = 1;
        click7 = 1;
        //click8 = 1;
        click9 = 1;
        click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }

    if ( mouseX > 368.9* displayWidth/588 && mouseX < 424.9* displayWidth/588 && mouseY > 5.2* displayHeight/16 && mouseY < 7.2* displayHeight/16)
    {
      if (click9 == 1)
      {

        fill(205, 133, 63);
        rect(displayWidth/5.5+ (4* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(8, hole.get(8)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(8) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(8), 10+ displayWidth/5.5 + 4*displayWidth/8.6, 40 + displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(8) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click9 -= 1;
      }
      else if (click9 == 0)
      {
        controli();
        click9 += 1;
        click1 = 1;
        click2 = 1;
        click3 = 1;
        click4 = 1;
        click5 = 1;
        click6 = 1;
        click7 = 1;
        click8 = 1;
        //click9 = 1;
        click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }


    if ( mouseX > 368.9* displayWidth/588 && mouseX < 424.9* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
    {
      if (click10 == 1)
      {   

        fill(205, 133, 63);
        rect(displayWidth/5.5+ (4* displayWidth/8.6), 3*(displayHeight/4), displayWidth/18, displayHeight/8);
        //rect(displayWidth/5.5+ (0* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(9, hole.get(9)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(9) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(9), 10+ displayWidth/5.5 + 4*displayWidth/8.6, 40 + 6*displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(9) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click10 -= 1;
      }
      else if (click10 == 0 )
      {
        controlj();
        click10 += 1;
        click1 = 1;
        click2 = 1;
        click3 = 1;
        click4 = 1;
        click5 = 1;
        click6 = 1;
        click7 = 1;
        click8 = 1;
        click9 = 1;
        //click10 = 1;
        click11 = 1;
        click12 = 1;
      }
    }


    if ( mouseX > 31*displayWidth/42 && mouseX < 5*displayWidth/6 && mouseY > 5.2* displayHeight/16 && mouseY < 7.2* displayHeight/16)
    {
      if (click11 == 1)
      {
        fill(205, 133, 63);
        rect(displayWidth/5.5+ (5* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(10, hole.get(10)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(10) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(10), 10+ displayWidth/5.5 + 5*displayWidth/8.6, 40 + displayHeight/8);
        popStyle();
        // lower rect for hints
        if (hole.get(10) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click11 -= 1;
      }
      else if (click11 == 0 )
      {
        controlk();
        click11 += 1;
        click1 = 1;
        click2 = 1;
        click3 = 1;
        click4 = 1;
        click5 = 1;
        click6 = 1;
        click7 = 1;
        click8 = 1;
        click9 = 1;
        click10 = 1;
        //click11 = 1;
        click12 = 1;
      }
    }

    if ( mouseX > 429* displayWidth/588 && mouseX < 486* displayWidth/588 && mouseY > 9* displayHeight/16 && mouseY < 11* displayHeight/16)
    {
      if (click12 == 1)
      {


        fill(205, 133, 63);
        rect(displayWidth/5.5+ (5* displayWidth/8.6), 3*(displayHeight/4), displayWidth/18, displayHeight/8);
        //rect(displayWidth/5.5+ (0* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
        hole.set(11, hole.get(11)+1);
        c1 += 1;
        println("Counter 1 = " + c1);
        println("a = " + hole.get(11) );
        pushStyle();
        textSize(30);
        fill(0);
        text(hole.get(11), 10+ displayWidth/5.5 + 5*displayWidth/8.6, 40 + 3*displayHeight/4);
        popStyle();
        // lower rect for hints
        if (hole.get(11) == 1 && c1 == c2)
        {
          if (turn == "PLAYER 1")
          {
            fill(205, 133, 63);
            rect(displayWidth/3, 0, 2*(displayWidth/3), displayHeight/10);
            //rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn2 + " Turn. ", 15 + displayWidth/3, 30);
            turn = turn2;
          }
          else if ( turn == "PLAYER 2")
          {
            fill(205, 133, 63);
            rect(0, 9*(displayHeight/10), 2*(displayWidth/3), displayHeight/10);
            fill(0);
            textSize(30);
            text("It's " + turn1 + "Turn. ", 15, 30 + 9*displayHeight/10);
          }
        }
        click12 -= 1;
      }
      else if (click12 == 0)
      {
        controll();
        click12 += 1;
        click1 = 1;
        click2 = 1;
        click3 = 1;
        click4 = 1;
        click5 = 1;
        click6 = 1;
        click7 = 1;
        click8 = 1;
        click9 = 1;
        click10 = 1;
        click11 = 1;
        // click12 = 1;
      }
    }
  }



  //Carrying for Hole a
  //if (mouseX > displayWidth/5.5 + 0* displayWidth/8.6 && mouseX < displayWidth/5.5 + 0* displayWidth + displayWidth/18 && mouseY > displayHeight/8 && mouseY < displayHeight/4)
  void controla()
  {
    c1 = 0;
    pushStyle();
    fill(205, 133, 63);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5 + 0* displayWidth/8.6, displayHeight/8, displayWidth/18, displayHeight/8);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    image(ayo_img[3], 7* displayWidth/48, 10+displayHeight/4, displayWidth/8, displayHeight/4);
    //rect((displayWidth/6) - 20, displayHeight/4, displayWidth/8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(0) + " stones by puttting a stone in each successive", 5+displayWidth/3, 15);
    text("pocket in the anti-clockwise direction.", 5 + displayWidth/3, 35);
    c2 = hole.get(0);
    println("Counter 2 " + c2);
    hole.set(0, 0);
    textSize(30);
    text(hole.get(0), 10+ displayWidth/5.5 + 0*displayWidth/8.6, 40 + displayHeight/8);
    popStyle();
  }

  //Carrying for Hole b
  //else if (mouseX > displayWidth/5.5 + 0* displayWidth/8.6 && mouseX < displayWidth/5.5 + 0* displayWidth + displayWidth/18 &&  mouseY > 3*displayHeight/4 && mouseY < 7*displayHeight/8)
  void controlb()
  {
    c1 = 0;
    pushStyle();
    fill(206, 133, 63);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5 + 0*displayWidth/8.6, 3*displayHeight/4, displayWidth/18, displayHeight/8);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    image(ayo_img[3], 7*displayWidth/48, 5+displayHeight/2, displayWidth/8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(1) + " stones by puttting a stone in each successive", 5, 15+9*displayHeight/10);
    text("pocket in the anti-clockwise direction.", 5, 35+9*displayHeight/10);
    c2 = hole.get(1);
    println(c2);
    hole.set(1, 0);
    textSize(30);
    text(hole.get(1), 10+displayWidth/5.5+ 0*displayWidth/8.6, 40+ 6*displayHeight/8);
    popStyle();
  }



  //Carrying for Hole c
  //    if (mouseX > displayWidth/5.5 + 1* displayWidth/8.6 && mouseX < displayWidth/5.5 + 1* displayWidth + displayWidth/18 && mouseY > displayHeight/8 && mouseY < displayHeight/4)
  void controlc()
  {
    c1 = 0;
    pushStyle();
    fill(205, 133, 63);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5 + 1* displayWidth/8.6, displayHeight/8, displayWidth/18, displayHeight/8);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    image(ayo_img[3], 13* displayWidth/48, 10+displayHeight/4, displayWidth/8, displayHeight/4);
    //rect((displayWidth/6) - 20, displayHeight/4, displayWidth/8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(2) + " stones by puttting a stone in each successive", 5+displayWidth/3, 15);
    text("pocket in the anti-clockwise direction.", 5 + displayWidth/3, 35);
    c2 = hole.get(2);
    println("Counter 2 " + c2);
    hole.set(2, 0);
    textSize(30);
    text(hole.get(2), 10+ displayWidth/5.5 + 1*displayWidth/8.6, 40 + displayHeight/8);
    popStyle();
  }

  //Carrying for Hole d
  //else if (mouseX > displayWidth/5.5 + 1* displayWidth/8.6 && mouseX < displayWidth/5.5 + 1* displayWidth + displayWidth/18 && mouseY > 3*displayHeight/4 && mouseY < 7*displayHeight/8)
  void controld()
  {
    c1 = 0;
    pushStyle();
    fill(206, 133, 63);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5 + 1*displayWidth/8.6, 3*displayHeight/4, displayWidth/18, displayHeight/8);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    image(ayo_img[3], 12.8*displayWidth/48, 5+displayHeight/2, displayWidth/8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(3) + " stones by puttting a stone in each successive", 5, 15+9*displayHeight/10);
    text("pocket in the anti-clockwise direction.", 5, 35+9*displayHeight/10);        
    c2 = hole.get(3);
    println(c2);
    hole.set(3, 0);
    textSize(30);
    text(hole.get(3), 10+displayWidth/5.5+ 1*displayWidth/8.6, 40+ 6*displayHeight/8);
    popStyle();
  }

  //Carrying for Hole e
  //  if (mouseX > displayWidth/5.5 + 2* displayWidth/8.6 && mouseX < displayWidth/5.5 + 2* displayWidth + displayWidth/18 && mouseY > displayHeight/8 && mouseY < displayHeight/4)
  void controle()
  {
    c1 = 0;
    pushStyle();
    fill(205, 133, 63);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    rect((displayWidth/5.5 + 2* displayWidth/8.6), displayHeight/8, displayWidth/18, displayHeight/8);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    image(ayo_img[3], 18.5* displayWidth/48, 10+displayHeight/4, displayWidth/8.8, displayHeight/4);
    //rect((displayWidth/6) - 20, displayHeight/4, displayWidth/8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(4) + " stones by puttting a stone in each successive", 5+displayWidth/3, 15);
    text("pocket in the anti-clockwise direction.", 5 + displayWidth/3, 35);
    c2 = hole.get(4);
    println("Counter 2 " + c2);
    hole.set(4, 0);
    textSize(30);
    text(hole.get(4), 10+ displayWidth/5.5 + 2*displayWidth/8.6, 40 + displayHeight/8);
    popStyle();
  }

  //Carrying for Hole f
  //else if (mouseX > displayWidth/5.5 + 2* displayWidth/8.6 && mouseX < displayWidth/5.5 + 2* displayWidth + displayWidth/18 && mouseY > 3*displayHeight/4 && mouseY < 7*displayHeight/8)
  void controlf()
  {
    c1 = 0;
    pushStyle();
    fill(206, 133, 63);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5 + 2*displayWidth/8.6, 3*displayHeight/4, displayWidth/18, displayHeight/8);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    image(ayo_img[3], 18.4*displayWidth/48, 5+displayHeight/2, displayWidth/8.8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(5) + " stones by puttting a stone in each successive", 5, 15+9*displayHeight/10);
    text("pocket in the anti-clockwise direction.", 5, 35+9*displayHeight/10);
    c2 = hole.get(5);
    println(c2);
    hole.set(5, 0);
    textSize(30);
    text(hole.get(5), 10+displayWidth/5.5+ 2*displayWidth/8.6, 40+ 6*displayHeight/8);
    popStyle();
  }


  //Carrying for Hole g
  //  if (mouseX > displayWidth/5.5 + 3* displayWidth/8.6 && mouseX < displayWidth/5.5 + 3* displayWidth + displayWidth/18 && mouseY > displayHeight/8 && mouseY < displayHeight/4)
  void controlg()
  {
    c1 = 0;
    pushStyle();
    fill(205, 133, 63);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5 + 3* displayWidth/8.6, displayHeight/8, displayWidth/18, displayHeight/8);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    image(ayo_img[3], 24.2* displayWidth/48, 10+displayHeight/4, displayWidth/8.8, displayHeight/4);
    //rect((displayWidth/6) - 20, displayHeight/4, displayWidth/8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(6) + " stones by puttting a stone in each successive", 5+displayWidth/3, 15);
    text("pocket in the anti-clockwise direction.", 5 + displayWidth/3, 35);
    c2 = hole.get(6);
    println("Counter 2 " + c2);
    hole.set(6, 0);
    textSize(30);
    text(hole.get(6), 10+ displayWidth/5.5 + 3*displayWidth/8.6, 40 + displayHeight/8);
    popStyle();
  }

  //Carrying for Hole h
  //else if (mouseX > displayWidth/5.5 + 3* displayWidth/8.6 && mouseX < displayWidth/5.5 + 3* displayWidth + displayWidth/18 && mouseY > 3*displayHeight/4 && mouseY < 7*displayHeight/8)
  void controlh()
  {
    c1 = 0;
    pushStyle();
    fill(206, 133, 63);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5 + 3*displayWidth/8.6, 3*displayHeight/4, displayWidth/18, displayHeight/8);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    image(ayo_img[3], 24.1*displayWidth/48, 5+displayHeight/2, displayWidth/8.8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(7) + " stones by puttting a stone in each successive", 5, 15+9*displayHeight/10);
    text("pocket in the anti-clockwise direction.", 5, 35+9*displayHeight/10);
    c2 = hole.get(7);
    println(c2);
    hole.set(7, 0);
    textSize(30);
    text(hole.get(7), 10+displayWidth/5.5+ 3*displayWidth/8.6, 40+ 6*displayHeight/8);
    popStyle();
  }



  //Carrying for Hole i
  //  if (mouseX > displayWidth/5.5 + 4* displayWidth/8.6 && mouseX < displayWidth/5.5 + 4* displayWidth + displayWidth/18 && mouseY > displayHeight/8 && mouseY < displayHeight/4)
  void controli()
  {
    c1 = 0;
    pushStyle();
    fill(205, 133, 63);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5 + 4* displayWidth/8.6, displayHeight/8, displayWidth/18, displayHeight/8);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    image(ayo_img[3], 29.8* displayWidth/48, 10+displayHeight/4, displayWidth/8.8, displayHeight/4);
    //rect((displayWidth/6) - 20, displayHeight/4, displayWidth/8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(8) + " stones by puttting a stone in each successive", 5+displayWidth/3, 15);
    text("pocket in the anti-clockwise direction.", 5 + displayWidth/3, 35);
    c2 = hole.get(8);
    println("Counter 2 " + c2);
    hole.set(8, 0);
    textSize(30);
    text(hole.get(8), 10+ displayWidth/5.5 + 4*displayWidth/8.6, 40 + displayHeight/8);
    popStyle();
  }

  //Carrying for Hole j
  //else if (mouseX > displayWidth/5.5 + 4* displayWidth/8.6 && mouseX < displayWidth/5.5 + 4* displayWidth + displayWidth/18 && mouseY > 3*displayHeight/4 && mouseY < 7*displayHeight/8)
  void controlj()
  {
    c1 = 0;
    pushStyle();
    fill(206, 133, 63);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5 + 4*displayWidth/8.6, 3*displayHeight/4, displayWidth/18, displayHeight/8);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    image(ayo_img[3], 29.7*displayWidth/48, 5+displayHeight/2, displayWidth/8.8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(9) + " stones by puttting a stone in each successive", 5, 15+9*displayHeight/10);
    text("pocket in the anti-clockwise direction.", 5, 35+9*displayHeight/10);
    c2 = hole.get(9);
    println(c2);
    hole.set(9, 0);
    textSize(30);
    text(hole.get(9), 10+displayWidth/5.5+ 4*displayWidth/8.6, 40+ 6*displayHeight/8);
    popStyle();
  }


  //Carrying for Hole k
  //  if (mouseX > displayWidth/5.5 + 5* displayWidth/8.6 && mouseX < displayWidth/5.5 + 5* displayWidth + displayWidth/18 && mouseY > displayHeight/8 && mouseY < displayHeight/4  )
  void controlk()
  {


    c1 = 0;
    pushStyle();
    fill(205, 133, 63);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5 + 5* displayWidth/8.6, displayHeight/8, displayWidth/18, displayHeight/8);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    image(ayo_img[3], 35.4* displayWidth/48, 10+displayHeight/4, displayWidth/8.8, displayHeight/4);
    //rect((displayWidth/6) - 20, displayHeight/4, displayWidth/8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(10) + " stones by puttting a stone in each successive", 5, 15+9*displayHeight/10);
    text("pocket in the anti-clockwise direction.", 5, 35+9*displayHeight/10);
    c2 = hole.get(10);
    println("Counter 2 " + c2);
    hole.set(10, 0);
    textSize(30);
    text(hole.get(10), 10+ displayWidth/5.5 + 5*displayWidth/8.6, 40 + displayHeight/8);
    popStyle();
  }

  //Carrying for Hole l
  //else if (mouseX > displayWidth/5.5 + 5* displayWidth/8.6 && mouseX < displayWidth/5.5 + 5* displayWidth + displayWidth/18 && mouseY > 3*displayHeight/4 && mouseY < 7*displayHeight/8)
  void controll()
  {
    c1 = 0;
    pushStyle();
    fill(206, 133, 63);
    rect(displayWidth/3, 0, 2* displayWidth/3, displayHeight/10);
    rect(displayWidth/5.5 + 5*displayWidth/8.6, 3*displayHeight/4, displayWidth/18, displayHeight/8);
    rect(0, 9*displayHeight/10, 2*displayWidth/3, displayHeight/10);
    image(ayo_img[3], 35.3*displayWidth/48, 5+displayHeight/2, displayWidth/8.8, displayHeight/4);
    popStyle();

    pushStyle();
    fill(0);
    textSize(15);
    text("Now, distribute the " + hole.get(11) + " stones by puttting a stone in each successive", 5, 15+9*displayHeight/10);
    text("pocket in the anti-clockwise direction.", 5, 35+9*displayHeight/10);
    c2 = hole.get(11);
    println(c2);
    hole.set(11, 0);
    textSize(30);
    text(hole.get(11), 10+displayWidth/5.5+ 5*displayWidth/8.6, 40+ 6*displayHeight/8);
    popStyle();
  }




  void control()
  {
    // for banks
    if (mousePressed)
    {
      if (mouseX > displayWidth/ 28 && mouseX < 3*displayWidth/28 && mouseY > displayHeight/4 && mouseY < 3*displayHeight/8)
      {    
        P1 += 4;
        pushStyle();
        fill(0);
        textSize(35);
        image(ayo_img[3], 0, 3*displayHeight/8, displayWidth/7, displayHeight/4);
        text( " X" + P1, 20+ displayWidth/68, 8* displayHeight/16);
        textSize(30);
        text(P1, 10+ displayWidth/28, 40+displayHeight/4);
        popStyle(); 

        pushStyle();
        fill(206, 133, 63);
        noStroke();
        rect(displayWidth/28, displayHeight/4, displayWidth/14, displayHeight/8);
        image(ayo_img[2], displayWidth/45, 8*displayHeight/18, 20, 20);
        popStyle();
      }
      else if (mouseX > 25*displayWidth/28 && mouseX < 55* displayWidth/56 && mouseY > displayHeight/4 && mouseY < 3*displayHeight/8)
      {    
        P2 += 4;
        pushStyle();
        fill(0);
        noStroke();
        textSize(35);
        image(ayo_img[3], 6*(displayWidth/7), 3*(displayHeight/8), displayWidth/7, displayHeight/4);
        text( "X" + P2, 20+ 60*displayWidth/68, 8* displayHeight/16);
        textSize(30);
        text(P2, 10+ 25*displayWidth/28, 40+displayHeight/4);
        popStyle();

        pushStyle();
        fill(206, 133, 63);
        noStroke();
        rect(25*displayWidth/28, displayHeight/4, displayWidth/14, displayHeight/8);
        image(ayo_img[2], 39*displayWidth/45, 8*displayHeight/18, 20, 20);
        popStyle();
      }
    }
  }
}
