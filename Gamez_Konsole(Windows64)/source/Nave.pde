import apwidgets.*;


APMediaPlayer player1;
APMediaPlayer player2;
class Nave {
  float x;
  float y;
  float vel = 2;
  boolean choco;
  int tam = 10;
  PImage ship;
  PImage back1, back2;

  //  
  //    player1 = new APMediaPlayer(this);
  //   player1.setMediaFile("soundtrack1.mp3");
  //   player2 = new APMediaPlayer(this);
  //   player2.setMediaFile("soundtrack2.mp3");

  Nave(float x_, float y_) {
    x = x_;
    y = y_;
    choco = false;
    ship = loadImage("ship.png");
    back1 = loadImage("nebula_blue.png");
    back2 = loadImage("debris.png");
  }

  int verChoque(Misil m) {
    PVector p[] = new PVector[4];
    p[0] = new PVector(x, y);
    p[1] = new PVector(x, y+tam);
    p[2] = new PVector(x+tam, y);
    p[3] = new PVector(x+tam, y+tam);

    for (int i=0; i<4; i++) {
      if (p[i].x > m.getX() && p[i].x < m.getX()+m.getTam() &&
        p[i].y > m.getY() && p[i].y < m.getY()+m.getTam()) {
        if (m.getTipo() == 0) choco = true;
        return m.getTipo();
      }
    }
    return -1;
  }


  void reset() {
    choco = false;
  }

  boolean choco() {
    return choco;
  }

  void draw() {
    image(back1, 0, 0, displayWidth, displayHeight);
    image(back2, 0, 0, displayWidth, displayHeight);

    y = mouseY;
    image(ship, x, y, tam*4, tam*4);

    if (atractor>0) {
      noFill();
      float t = map(sin(frameCount/10.0), -1, 1, 4*tam, 10*tam);
      stroke(0, 255, 0);
      ellipse(x+tam/2, y+tam/2, t, t);
    }
  }

  //GETTERS AND SETTERS
  float getX() {
    return x;
  }
  float getY() {
    return y;
  }
}

